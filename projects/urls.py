from django.urls import path

from . import views

app_name = 'projects'

urlpatterns = [
	# post views
	path('', views.projects_list, name='list_projects'),
]