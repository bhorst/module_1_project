from django.shortcuts import render, redirect
from .models import Project


def projects_list(request):
    projects = Project.objects.all()
    return render(request, "projects/projects/list.html", {"projects": projects})


def redirect_view(request):
    return redirect("/projects/")
